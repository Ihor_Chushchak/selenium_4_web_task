package po;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.stream.Stream;

import utils.ConfigReader;
import utils.DriverManager;

import static org.testng.Assert.assertEquals;

public class GmailAndAuthorizationPageTest {

    private static WebDriver driver;
    private static  WebDriverWait wait;

    private static final Logger LOG = LogManager.getLogger(GmailAndAuthorizationPageTest.class);

    @DataProvider(parallel = true)
    public Iterator<Object[]> users(){
        return Stream.of(
                new Object[]{ConfigReader.read("SOME_EMAIL_LOGIN_1"),ConfigReader.read("SOME_EMAIL_PASSWORD_1")},
                new Object[]{ConfigReader.read("SOME_EMAIL_LOGIN_2"),ConfigReader.read("SOME_EMAIL_PASSWORD_2")},
                new Object[]{ConfigReader.read("SOME_EMAIL_LOGIN_3"),ConfigReader.read("SOME_EMAIL_PASSWORD_3")},
                new Object[]{ConfigReader.read("SOME_EMAIL_LOGIN_4"),ConfigReader.read("SOME_EMAIL_PASSWORD_4")},
                new Object[]{ConfigReader.read("SOME_EMAIL_LOGIN_5"),ConfigReader.read("SOME_EMAIL_PASSWORD_5")}).iterator();
    }

    @BeforeMethod
    static void initializeObjects() {
        LOG.info("Open web-driver");
        DriverManager.getDriver().get(ConfigReader.read("HOME_PAGE"));
        LOG.info("Moving to login_submit page");
        driver = DriverManager.getDriver();
    }

    @Test(dataProvider = "users")
    void openGmailAndLoginTest(String userEmail, String password){
        GmailAndAuthorizationPage gmailPage = new GmailAndAuthorizationPage(driver);

        gmailPage.fillLogin(userEmail);

        gmailPage.clickLoginNextButtom();

        gmailPage.fillPassword(password);

        gmailPage.clickPasswordNextButton(driver,wait);
        assertEquals( driver.findElement(By.cssSelector("div.aic div[role='button']")).getText(),"Написати");

        gmailPage.clickComposeEmailButton();
        assertEquals(driver.findElement(By.xpath("/html/body/div[19]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[2]/div/div[2]/div/div/div/div/table/tbody/tr/td[1]/div/h2/div[2]")).getText(),"Нове повідомлення");

        gmailPage.fillToField(userEmail);

        gmailPage.fillSubjectField(ConfigReader.read("EMAIL_SUBJECT"));

        gmailPage.fillMessageField(ConfigReader.read("EMAIL_MESSAGE"));

        assertEquals(driver.findElement(By.cssSelector("#\\:kx > h2 > div.aYF")).getText(),ConfigReader.read("EMAIL_SUBJECT"));

        gmailPage.clickSendEmailButton();
        assertEquals(driver.findElement(By.xpath("//*[@id='link_vsm']")).getText(),"Переглянути повідомлення");

        gmailPage.clickOpenMessageButton();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        LocalDateTime now = LocalDateTime.now();
        assertEquals(dtf.format(now) + " (0 хвилин тому)", driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]" +
                "/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div/table/tr/td[1]/div[2]/div[2]/div" +
                "/div[3]/div/div/div/div/div/div[1]/div[2]/div[1]/table/tbody/tr[1]/td[2]/div/span[2]")).getText());
        assertEquals(gmailPage.checkSendButton(),"0");

        gmailPage.clickDeleteEmailButton();
        assertEquals(driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[1]/div[4]/div[1]/div/div[3]/div/div/div[2]/span/span[1]")).getText(),"Ланцюжок повідомлень переміщено в кошик.");
    }

    @AfterMethod
    static void closeResources() {
        driver.quit();
        DriverManager.closeResources();
    }

}


