package po;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailAndAuthorizationPage{

    private static final Logger LOG = LogManager.getLogger(GmailAndAuthorizationPage.class);

    @FindBy(css = "#identifierId")
    private WebElement loginInput;

    @FindBy(xpath = "//*[@id=\"identifierNext\"]/span")
    private WebElement loginNextButton;

    @FindBy(xpath = "//*[@id=\"password\"]/div[1]/div/div[1]/input")
    private WebElement passwordInput;

    @FindBy(css = "#passwordNext")
    private WebElement passwordNextButton;

    @FindBy(css = "div.aic div[role='button']")
    private WebElement composeEmailButton;

    @FindBy(css = "textarea[name='to']")
    private WebElement toField;

    @FindBy(css = "input[name='subjectbox']")
    private WebElement subjectField;

    @FindBy(xpath = "//div[@class='Ar Au']/*[@role='textbox']")
    private WebElement messageField;

    @FindBy(xpath = "//div[@class='dC']/*[@role='button']")
    private WebElement sendEmailButton;

    @FindBy(xpath = "//*[@id='link_vsm']")
    private WebElement openMessageButton;

    @FindBy(xpath = "//*[@id=\":j7\"]/div/div[2]/span/a")
    private WebElement chooseSendButton;

    @FindBy(xpath = "//*[@id=\":4\"]/div[2]/div[1]/div/div[2]/div[3]")
    private WebElement deleteEmailButton;

    public GmailAndAuthorizationPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void fillLogin(String login) {
        LOG.info("Entering login");
        loginInput.sendKeys(login);
    }

    public void clickLoginNextButtom() {
        LOG.info("Moving to password_submit page");
        loginNextButton.click();
    }

    public void fillPassword(String password) {
        LOG.info("Entering password");
        passwordInput.sendKeys(password);
    }

    public void clickPasswordNextButton(WebDriver driver,WebDriverWait wait) {
        LOG.info("Moving to gmail_main page");
        wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(passwordNextButton));
        passwordNextButton.click();
    }

    public void clickComposeEmailButton() {
        LOG.info("Clicking compose_email button");
        composeEmailButton.click();
    }

    public void fillToField(String to) {
        LOG.info("Filling \"To\" filed");
        toField.sendKeys(to);
    }

    public void fillSubjectField(String subject) {
        LOG.info("Filling \"Subject\" filed");
        subjectField.sendKeys(subject);
    }

    public void fillMessageField(String message) {
        LOG.info("Filling \"Message\" filed");
        messageField.sendKeys(message);
    }

    public void clickSendEmailButton() {
        LOG.info("Clicking send_email button");
        sendEmailButton.click();
    }

    public void clickOpenMessageButton() {
        LOG.info("Clicking open_sended_email button");
        openMessageButton.click();
    }

    public String checkSendButton(){
     return chooseSendButton.getAttribute("tabindex");
    }

    public void clickDeleteEmailButton(){
        LOG.info("Clicking delete_sended_email button");
        deleteEmailButton.click();
    }
}
