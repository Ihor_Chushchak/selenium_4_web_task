package utils;
import java.util.ResourceBundle;
public abstract class ConfigReader {

        private static ResourceBundle bundle = ResourceBundle.getBundle("config");

        public static String read(String key) {
            return bundle.getString(key);

    }

}
