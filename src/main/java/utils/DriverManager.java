package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class DriverManager {

    private static ThreadLocal<WebDriver> driver = new ThreadLocal();

    private DriverManager(){}

    public static WebDriver getDriver(){
        if (driver.get() == null){
            initializeObjects();
        }
        return driver.get();
    }

    public static void initializeObjects() {
        System.setProperty(ConfigReader.read("WEB_DRIVER_NAME"),ConfigReader.read("PATH_TO_DRIVER"));
        driver.set(new ChromeDriver());
        driver.get().manage().timeouts()
                .implicitlyWait(Long.parseLong(ConfigReader.read("DEFAULT_IMPLICITLY_WAIT_TIME")), TimeUnit.SECONDS);
    }

    public static void closeResources() {
        driver.get().quit();
        driver.set(null);
    }

//    public static void main(String[] args) {
//        System.out.println(ConfigReader.read("WEB_DRIVER_NAME"));
//    }

}
